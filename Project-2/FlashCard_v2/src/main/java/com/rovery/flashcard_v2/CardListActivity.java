package com.rovery.flashcard_v2;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.zip.Inflater;

public class CardListActivity extends Activity {

    private cardListFragmant Fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardlist);
        this.setTitle(R.string.card_list_activity_title);

        FragmentManager fragmentManager = getFragmentManager();
        Fragment = (cardListFragmant) fragmentManager.findFragmentById(R.id.fragmentContainer);

        if( Fragment == null)
        {
            Fragment = new cardListFragmant();
            fragmentManager.beginTransaction().add(R.id.fragmentContainer, Fragment).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.card_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId())
        {
            case R.id.action_addcard:
                this.Fragment.addCard();
                break;
            default:
                Toast.makeText(this, "Something else selected", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
