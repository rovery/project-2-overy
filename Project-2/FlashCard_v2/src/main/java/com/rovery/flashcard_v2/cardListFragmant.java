package com.rovery.flashcard_v2;




import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class cardListFragmant extends ListFragment {

    private static ArrayList<FlashCard> cardArrayList;
    private ArrayAdapter<FlashCard> arrayAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        cardArrayList = FlashCardCollection.get(getActivity()).getmFlashCards();

        arrayAdapter = new ArrayAdapter<FlashCard>(getActivity(), android.R.layout.simple_list_item_1, cardArrayList);
        setListAdapter( arrayAdapter );
    }

    public void addCard()
    {
        cardArrayList.add(0, new FlashCard());
        arrayAdapter.notifyDataSetChanged();

    }

    public void onResume()
    {
        super.onResume();
        arrayAdapter.notifyDataSetChanged();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
      return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
         FlashCard currentFlashCard = (FlashCard)getListAdapter().getItem(position);
         Intent cardActivityIntent = new Intent(getActivity(), CardActivity.class);
         cardActivityIntent.putExtra(CardActivity.UUID_FLASH_CARD, currentFlashCard.getID());
         startActivity(cardActivityIntent);

    }
}
