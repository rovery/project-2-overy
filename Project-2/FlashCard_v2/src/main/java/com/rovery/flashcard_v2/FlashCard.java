package com.rovery.flashcard_v2;

import java.util.UUID;

/**
 * Created by robby_000 on 10/3/13.
 */
public class FlashCard {

    private String mQustion;
    private String mAnswer;
    private UUID mID;

    public FlashCard(String mQuestion, String mAnswer)
    {
        this.mQustion = mQuestion;
        this.mAnswer = mAnswer;
        mID = UUID.randomUUID();
    }

    public FlashCard()
    {
       this.mAnswer = "defualt answer";
        this.mQustion = "default question";
        mID = UUID.randomUUID();
    }
    public String getmQustion()
    {
        return mQustion;
    }
    public String getmAnswer()
    {
        return mAnswer;
    }
    public void setmQuestion(String mQuestion)
    {
        this.mQustion = mQuestion;
    }
    public void setmAnswer(String mAnswer)
    {
        this.mAnswer = mAnswer;
    }
    public UUID getID()
    {
        return mID;
    }
    public void updatFlashCard(boolean isQuestion, String question_or_answer)
    {
        if(isQuestion)
        {
            setmQuestion(question_or_answer);
        }
        else
        {
            setmAnswer(question_or_answer);
        }
    }
    @Override
    public String toString()
    {
        return this.mQustion;
    }
}
