package com.rovery.flashcard_v2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by robby_000 on 9/11/13.
 */
public class EditActivity extends Activity {
    public static final String FLASH_CARD_DATA ="Answer" + R.string.package_name;
    public static final String FLASH_CARD_CHANGED="if changed" + R.string.package_name;


    private Button mCancelButton;
    private Button mSaveButton;
    private EditText mTextField;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        if((getIntent().getIntExtra(CardActivity.REQUEST_CODE, 0))== CardActivity.REQUEST_ANSWER)
            this.setTitle(R.string.edit_activity_answer_title);
        else this.setTitle(R.string.edit_activity_question_title);

        mTextField = (EditText) findViewById(R.id.Edit_Text_Answer_Question);
        mTextField.setText(getIntent().getStringExtra(FLASH_CARD_DATA));


        mCancelButton = (Button) findViewById(R.id.Cancel_Button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSaveButton = (Button) findViewById(R.id.Save_Button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(FLASH_CARD_CHANGED, mTextField.getText().toString());
                setResult(Activity.RESULT_OK, returnIntent);
                Toast.makeText(EditActivity.this, "were here", Toast.LENGTH_LONG).show();
                finish();

            }
        });
    }
}
