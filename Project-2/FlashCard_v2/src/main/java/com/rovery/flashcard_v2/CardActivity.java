package com.rovery.flashcard_v2;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.UUID;

public class CardActivity extends Activity {

    public static final String UUID_FLASH_CARD = "UUID for flash card in card activity";
    public static final String REQUEST_CODE = "the request code for the ansewr or question";
    public static final int REQUEST_ANSWER = 1;
    public static final int REQUEST_QUESTION = 0;



    private Button mEditAnswerButton;
    private Button mEditQuestionButton;
    private TextView mAnswerTextView;
    private TextView mQuestionTextView;

    private FlashCard mFlashCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        this.setTitle(R.string.card_activity_title);

        mAnswerTextView = (TextView) findViewById(R.id.answer_text_view);
        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);

        if(savedInstanceState != null)
        {
            FlashCardCollection cards = FlashCardCollection.get(this.getBaseContext());
            mFlashCard = cards.getFlashCard((UUID)savedInstanceState.getSerializable(UUID_FLASH_CARD));
        }
        else
        {
            FlashCardCollection cards = FlashCardCollection.get(this.getBaseContext());
            mFlashCard = cards.getFlashCard((UUID)getIntent().getSerializableExtra(UUID_FLASH_CARD));
        }

        mAnswerTextView.setText(mFlashCard.getmAnswer());
        mQuestionTextView.setText(mFlashCard.getmQustion());

        mEditAnswerButton = (Button) findViewById(R.id.edit_answer_button);
        mEditAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editAnswerIntent = new Intent(CardActivity.this, EditActivity.class);
                editAnswerIntent.putExtra(EditActivity.FLASH_CARD_DATA, mAnswerTextView.getText().toString());
                editAnswerIntent.putExtra( REQUEST_CODE, REQUEST_ANSWER);
                startActivityForResult(editAnswerIntent, REQUEST_ANSWER);

            }
        });
        mEditQuestionButton = (Button) findViewById(R.id.edit_question_button);
        mEditQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editQuestionIntent = new Intent(CardActivity.this, EditActivity.class);
                editQuestionIntent.putExtra(EditActivity.FLASH_CARD_DATA, mQuestionTextView.getText().toString());
                editQuestionIntent.putExtra( REQUEST_CODE, REQUEST_QUESTION);
                startActivityForResult(editQuestionIntent, REQUEST_QUESTION);

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        mFlashCard.updatFlashCard( requestCode == 0 , data.getStringExtra(EditActivity.FLASH_CARD_CHANGED));
        if (requestCode == 0 && resultCode == RESULT_OK) {
            mQuestionTextView.setText(data.getExtras().getString(EditActivity.FLASH_CARD_CHANGED));
        }
        if (requestCode == 1 && resultCode == RESULT_OK) {
            mAnswerTextView.setText(data.getExtras().getString(EditActivity.FLASH_CARD_CHANGED));
        }


    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable(CardActivity.UUID_FLASH_CARD, mFlashCard.getID());
    }



}

