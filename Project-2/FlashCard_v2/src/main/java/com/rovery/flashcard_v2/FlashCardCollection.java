package com.rovery.flashcard_v2;

import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by robby_000 on 10/3/13.
 */
public class FlashCardCollection {
    private static FlashCardCollection sFlashCardCollection;
    private Context mCardListContect;

    private ArrayList<FlashCard> mCardArrayList;

    private FlashCardCollection(Context appContext) {
        mCardListContect = appContext;
        mCardArrayList = new ArrayList<FlashCard>();
        for(int i = 0; i<10 ; i++)
        {
            String question = i + " + " + (i+1) + " = ?";
            String answer ="" + (i + i+1);
            mCardArrayList.add(new FlashCard(question, answer));
        }

    }

    public static FlashCardCollection get(Context c) {
        if (sFlashCardCollection == null) {
            sFlashCardCollection = new FlashCardCollection(c.getApplicationContext());
        }
        return sFlashCardCollection;
    }

    public ArrayList<FlashCard> getmFlashCards() {
        return mCardArrayList;
    }


    public FlashCard getFlashCard(UUID id) {
        for (FlashCard fc : mCardArrayList) {
            if (fc.getID().equals(id))
                return fc;
        }
        return null;
    }
}
